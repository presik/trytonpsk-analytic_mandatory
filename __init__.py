# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .invoice import InvoiceLine
from .sale import SaleLine
from .purchase import PurchaseLine
from .tax import (TaxesAnalytic, TaxesAnalyticStart, PrintTaxesAnalytic)


def register():
    Pool.register(
        InvoiceLine,
        PurchaseLine,
        SaleLine,
        TaxesAnalyticStart,
        module='analytic_mandatory', type_='model')
    Pool.register(
        PrintTaxesAnalytic,
        module='analytic_mandatory', type_='report')
    Pool.register(
        TaxesAnalytic,
        module='analytic_mandatory', type_='wizard')
