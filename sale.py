# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import PoolMeta
from trytond.pyson import Eval


__all__ = ['SaleLine']


STATES = {
        'required': Eval('type') == 'line',
        'invisible': Eval('type') != 'line',
}

class SaleLine:
    __metaclass__ = PoolMeta
    __name__ = "sale.line"

    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()
        cls.analytic_accounts.states = STATES
