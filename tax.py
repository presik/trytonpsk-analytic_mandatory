# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.model import fields, ModelView
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateAction, Button
from trytond.report import Report

__all__ = ['TaxesAnalytic', 'TaxesAnalyticStart', 'PrintTaxesAnalytic']

_ZERO = Decimal('0.0')


class TaxesAnalyticStart(ModelView):
    'Taxes Analytic Start'
    __name__ = 'analytic_mandatory.print_taxes_analytic.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('start_date', '<=', (Eval('end_period'), 'start_date')),
            ], depends=['fiscalyear', 'end_period'], required=True)
    end_period = fields.Many2One('account.period', 'End Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('start_date', '>=', (Eval('start_period'), 'start_date'))
            ],
        depends=['fiscalyear', 'start_period'], required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None


class PrintTaxesAnalytic(Wizard):
    'Print Taxes Analytic'
    __name__ = 'analytic_mandatory.print_taxes_analytic'
    start = StateView('analytic_mandatory.print_taxes_analytic.start',
        'analytic_mandatory.print_taxes_analytic_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateAction('analytic_mandatory.report_taxes_analytic')

    def do_print_(self, action):

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'start_period': self.start.start_period.id,
            'end_period': self.start.end_period.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class TaxesAnalytic(Report):
    __name__ = 'analytic_mandatory.taxes_analytic_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(TaxesAnalytic, cls).get_context(records, data)
        pool = Pool()
        Period = pool.get('account.period')
        Company = pool.get('company.company')
        FiscalYear = pool.get('account.fiscalyear')
        Tax = pool.get('account.tax')
        MoveLine = pool.get('account.move.line')
        company = Company(data['company'])

        start_period = Period(data['start_period'])
        end_period = Period(data['end_period'])

        taxes = Tax.search([], order=[('invoice_account.code', 'ASC')])
        #taxes_accounts = {}

        account_taxes_ids = []
        for tax in taxes:
             account_taxes_ids.extend([
                    tax.invoice_account.id,
                    tax.credit_note_account.id
            ])

        periods = Period.search([
                ('fiscalyear', '=', data['fiscalyear']),
                ('start_date', '>=', start_period.start_date),
                ('end_date', '<=', end_period.end_date),
                ])
        period_ids = [p.id for p in periods]

        lines = MoveLine.search([
            ('account', 'in', account_taxes_ids),
            ('move.period', 'in', period_ids),
        ], order=[('date', 'ASC')])

        def _get_lines_analytic(analytic_account=None):
            val = {
                'lines': {},
                'sum_credit': [],
                'sum_debit': [],
                'sum_base': [],
            }
            if analytic_account:
                val['name'] = analytic_account.name
            else:
                val['name'] = 'GENERAL'
            return val

        def _get_data_tax(line):
            val = {
                'account_code': line.account.code,
                'account_name': line.account.name,
                'debit': [line.debit],
                'credit': [line.credit],
                'base': [],
                'percentage': '',
            }
            return val

        analytics = {}
        for line in lines:
            if line.analytic_lines:
                for aline in line.analytic_lines:
                    if aline.account not in analytics.keys():
                        analytics[aline.account] = _get_lines_analytic(aline.account)
                        analytics[aline.account]['lines'][line.account.id] = _get_data_tax(line)
                    else:
                        analytics[aline.account]['lines'][line.account.id]['debit'].append(line.debit)
                        analytics[aline.account]['lines'][line.account.id]['credit'].append(line.credit)
            else:
                if 'GENERAL' not in analytics.keys():
                    analytics['GENERAL'] = _get_lines_analytic()
                    analytics['GENERAL'] = _get_data_tax(line)
                else:
                    analytics['GENERAL']['lines'][line.account.id]['debit'].append(line.debit)
                    analytics['GENERAL']['lines'][line.account.id]['credit'].append(line.credit)

        """
            line_id = line.account.id
            if line_id not in targets.keys():
                targets[line_id] = taxes_accounts[line_id]
            line_ = cls.get_tax_reversed(line)
            targets[line_id]['lines'].append(line_)
            targets[line_id]['sum_base'].append(line_.base)
            targets[line_id]['sum_amount'].append(line_.tax_amount)
        """

        report_context['start_period'] = start_period.name
        report_context['end_period'] = end_period.name
        report_context['fiscalyear'] = FiscalYear(data['fiscalyear']).name
        report_context['company'] = company.rec_name
        report_context['records'] = []
        return report_context

    @classmethod
    def get_tax_reversed(cls, line):
        rate = None
        base = _ZERO
        amount = line.debit - line.credit
        if line.tax_lines:
            for tax_line in line.tax_lines:
                rate = tax_line.tax.rate
            if rate:
                base = amount / abs(rate)
                rate = rate * 100


        #TODO: Add smart compute deduced from taxes model
        setattr(line, 'base', base)
        setattr(line, 'tax_amount', amount)
        setattr(line, 'rate', rate)
        return line
